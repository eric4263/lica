module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: {
        'zh-CN':   ['cn'],
        'zh-HK':   ['zh'],
        'zh-TW':   ['tw'],
        'en-US':   ['en'],
        'default': ['en']
      },
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/assets/_variables.sass"`
      },
    }
  },
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap(args => {
          args[0].title = 'lica';
          args[0].meta = [
            {
                property: 'viewport',
                content: 'width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no',
            }
        ];
      return args;
    })
  },
  publicPath: process.env.NODE_ENV === 'development' ? '/eric/lica_v2/' : '/eric/lica_v2/'
}


