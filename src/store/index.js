import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import axios from 'axios'
import global_ from '../api/Global.vue'
import i18n from '@/i18n.js' 
import isMobile from 'ismobilejs';
// import VueGtm from "vue-gtm";


Vue.prototype.GLOBAL = global_;
axios.defaults.baseURL=global_.BASE_URL;

Vue.use(Vuex)
Vue.use(VueCookies)




const storeData = {
	state: () => ({
		answers:[]
	}),
	mutations: {
		setStoreData(state,data){
			if (data == "reset") {
				state.answers=[]
			}else if (data == "prev") {
				state.answers.pop()
			} else{
				state.answers.push(data)
			}
		},
	},
	actions: {},
	getters: {}
}


const presetErrorMsg = {
	state: () => ({
		errorMsg:{
			isOpen: true,
			header: i18n.t('warning.connection.header'),
			content: i18n.t('warning.connection.content'),
			confirm__btn:  i18n.t('warning.connection.btn'),
			confirm__target: "login__again",
			cancel__btn: '',
		}
	}),
	mutations: {},
	actions: {},
	getters: {}
}

const storeUserQuestions = {
	state: () => ({
		userQuestions:[]
	}),
	mutations: {
		setStoreUserQuestions(state,data){
			state.userQuestions = data
		},
	},
	actions: {},
	getters: {}
}


const storeMyCamellia = {
	state: () => ({
		myCamelliaData:[]
	}),
	mutations: {
		setStoreMyCamellia(state,data){
			state.myCamelliaData = data
		},
	},
	actions: {},
	getters: {}
}


const storeGlobalDivisionData = {
	state: () => ({
		globalDivisionData:[]
	}),
	mutations: {
		setStoreGlobalDivisionData(state,data){
			state.globalDivisionData = data
		},
	},
	actions: {},
	getters: {}
}

const storeUltimateData = {
	state: () => ({
		ultimateData:[]
	}),
	mutations: {
		setStoreUltimateData(state,data){
			state.ultimateData = data
		},
	},
	actions: {},
	getters: {}
}


const storeDevelopmentSupportData = {
	state: () => ({
		developmentSupportData:[]
	}),
	mutations: {
		setDevelopmentSupportData(state,data){
			state.developmentSupportData = data
		},
	},
	actions: {},
	getters: {}
}

export default new Vuex.Store({
	state: {
		languagesList: [],
		isCompletedQuestion: false,
		isLoading: false,
		// isLoggedIn: false,
		isLocaleOpened: false,
		isMemberMenuOpened: false,
		isMaskOpened: false,
		isBackBtnOpened:false,
		isHomeBtnOpened:false,
		isLangBtnShow:false,
		isInQuestion:false,
		warningPop: {
			isOpen: false,
			header: '',
			content: '',
			confirm__btn: '',
			cancel__btn: '',
			
		},
		openRoleDetailPop: false,
		journeyType: '', // dream_job / my_journey
		currentSession:0,
		currentSessionTotalStep:[],
		totalFinishedStep:1,
		currentQuestion:0,
		ultimateStepCount:1,
		// nextStep:null,
		staff:{
			id : "",
			name: '',
			email: '',
			company: '',
			division: '',
			position: '',
			given_name: '',
			surname: '',
			preferred_language: '',
			country_code: '',
			country_name: '',
			display_name: '',
			employee_type: '',
		},
		start_role:{
			allPosition:[],
			name:'',
			id:'',
			division_id:'',
			beginning_start_role:{
				name:'',
				id:'',
				division_id:'',
			}
			
		},
		next_start_role:{
			name:'',
			id:'',
			division_id:'',
		},
		globalSelectedColor:'',
		localePrefix:'',
		positionsData:[],

		newActiveTime: null,
		oldActiveTime: null,

		isPopUpOpen : false,
		popUpData : {
			pic:'',
			title:'',
			name:'',
			desc:'',
		},

	},
	mutations: {

		togglePopup(state,data){
			state.isPopUpOpen = data
		},

		setPopupData(state,data){
			state.popUpData = {
				pic: data.pic,
				title: data.title,
				name: data.name,
				desc: data.desc,
				popText: data.popText,
			}
		},


		setOldActiveTime(state,data){
			state.oldActiveTime = data
		},


		toggleLocaleMenu(state,data){
			state.isLocaleOpened = data

		},
		toggleMemberMenu(state, data){
			state.isMemberMenuOpened = data
		},
		// staffLogIn(state){
			// 	state.isLoggedIn = true 
			// },
		changeJourneyType(state,data){
			state.journeyType = data 
		},

		setLanguagesList(state,data){
			state.languagesList.push(data)
		},
		setBackBtn(state,data){
			state.isBackBtnOpened = data 
		},
		setHomeBtn(state,data){
			state.isHomeBtnOpened = data 
		},
		setLangBtn(state,data){
			state.isLangBtnShow = data 
		},
		setCurrentSessionTotalStep(state,data){
			if(data == 'reset'){
				state.currentSessionTotalStep=[]
			}else{
				state.currentSessionTotalStep.push(data)
			}
		},
		setCurrentSession(state,data){
			if(data == 'next'){
				state.currentSession++
				state.totalFinishedStep ++
			}else if(data == 'prev'){
				state.currentSession--
				state.totalFinishedStep --
			}else if(data == 'reset'){
				state.currentSession = 0
				// state.totalFinishedStep = 1
			}
		},
		setCurrentQuestion(state,data){
			if(typeof data == 'number'){
				state.currentQuestion = data-1
			}else{
				if(data == 'next'){
					state.currentQuestion++
					state.totalFinishedStep ++
				}else if(data == 'prev'){
					state.currentQuestion--
					state.totalFinishedStep --
				}else if(data == 'reset'){
					state.currentQuestion = 0
					// state.totalFinishedStep = 1
				}
			}
		},
		resetTotalFinishedStep(state){
			state.totalFinishedStep = 1
		},
		setStaffData(state,data){
			state.staff = {
				id : data.employee_id || "",
				name: data.username.toUpperCase(),
				email: data.email,
				company: data.company,
				division: data.division,
				position: data.title,
				given_name: data.given_name.toUpperCase(),
				surname: data.surname.toUpperCase(),
				preferred_language: data.preferred_language,
				country_code: data.country_code,
				country_name: data.country_name,
				display_name: data.display_name.toUpperCase(),
				employee_type: data.employee_type,
			}

		},
		setLoading(state,data){
			state.isLoading = data
			document.body.style.overflow = state.isLoading ? 'hidden' : ''
			document.documentElement.style.overflow = state.isLoading ? 'hidden' : '';
		},
		openRoleDetailPop(state ,data){
			state.openRoleDetailPop = data 
		},
		openWarningPop(state,data){
			state.warningPop = data
		},
		setGlobalSelectColor(state,data){
			state.globalSelectedColor = data.replace("#",'')
		},
		updateLocale(state,data){
			state.localePrefix = data
			i18n.locale = data
		},
		setUltimateStepCount(state,data){
			state.ultimateStepCount = data
		},
		setStartRole(state,data){
			state.start_role={
				name:data.name,
				id:data.id,
				division_id:data.division_id,
				allPosition:data.allPosition,
				beginning_start_role: data.beginning_start_role || ''
			}
		},
		setNextStartRole(state,data){
			state.next_start_role={
				name:data.name,
				id:data.id,
				division_id:data.division_id,
				beginning_start_role: data.beginning_start_role || ''
			}
		},
		setPositionsData(state,data){
			state.positionsData.push(data)
		},
		setIsInQuestion(state,data){
			state.isInQuestion = data
		},
		setIsCompletedQuestion(state,data){
			state.isCompletedQuestion = data
		}
	},
	actions: {
		getProfile({commit, state}){
			// axios.get( state.localePrefix +'/member/profile/')
			axios.get('en/member/profile/')
			.then(resp => {
				// console.log(resp.data);
				this.commit('setStaffData', resp.data)

				if (this._vm.$gtm.enabled()) {
					this._vm.$gtm.trackEvent({
						userId: resp.data.employee_id,
						Market: resp.data.company
					});
				}
				
				let lang; 
				let isPreferredLanguageOnTheList = false;
				state.languagesList.forEach(language => {
					if (resp.data.preferred_language == language) {
						isPreferredLanguageOnTheList = true
					}
				});
				if (state.localePrefix == '') {
					if (resp.data.preferred_language == "en-US" || resp.data.preferred_language == "" || !isPreferredLanguageOnTheList) {
						lang = 'en'
					}else{
						lang = resp.data.preferred_language
					}
				}else{
					lang = state.localePrefix
				}
                this.commit('updateLocale', lang)
			}).catch(err => {
				console.log("err",err);
				if (err.response.status == 401) {
					this.commit('openWarningPop',state.presetErrorMsg.errorMsg)
				}else{
					let warnData = {
						isOpen: true,
						header: i18n.t('warning.other_error.header'),
						content: i18n.t('warning.other_error.content') + '\n' + error.response.data.code,
						confirm__btn: i18n.t('warning.connection.btn'),
						confirm__target: "/",
						cancel__btn: '',
					}
					this.commit('openWarningPop',warnData)
				}

			})
		},
	},
	modules: {
		storeData: storeData,
		storeUserQuestions: storeUserQuestions,
		storeMyCamellia: storeMyCamellia,
		storeGlobalDivisionData: storeGlobalDivisionData,
		storeUltimateData: storeUltimateData,
		storeDevelopmentSupportData: storeDevelopmentSupportData,
		presetErrorMsg: presetErrorMsg,
	}
})
